import data from '../constants/data.json';
import { ContactModel } from '../models/ContactModel';
export function getData(): ContactModel[] {
  return [...data];
}
export function updateData(updatedData: ContactModel) {
  for (let i = 0; i < data.length; i++) {
    if (data[i].id == updatedData.id) {
      data.splice(i, 1, updatedData);
      return;
    }
  }
}
