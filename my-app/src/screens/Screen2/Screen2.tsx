import { useState } from 'react';
import { ScrollView, View, StyleSheet, TextInput } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { ContactModel } from '../../models/ContactModel';
import Header from '../../components/Header';
import HeaderSide from '../../components/HeaderSide';
import ContactPhoto from '../../components/ContactPhoto';
import ContactEditInputSection from '../../components/ContactEditInputSection';
import { updateData } from '../../services/dataService';

function Screen2() {
  const navigation = useNavigation();
  const route = useRoute();
  const [contactData, setContactData] = useState<ContactModel>(
    route.params.contact
  );
  const inputs: {
    firstName: TextInput | null;
    lastName: TextInput | null;
    email: TextInput | null;
    phone: TextInput | null;
  } = {
    firstName: null,
    lastName: null,
    email: null,
    phone: null,
  };
  const handleSave = () => {
    if (contactData.firstName && contactData.lastName) {
      const updatedContact: ContactModel = {
        ...contactData,
        firstName: contactData.firstName,
        lastName: contactData.lastName,
        email: contactData.email,
        phone: contactData.phone,
      };
      updateData(updatedContact);
      navigation.goBack();
      route.params.handleRefresh();
    } else {
      console.log('validation unsuccessful');
    }
  };
  return (
    <ScrollView>
      <View>
        <Header
          left={{
            onClick: function (): void {
              console.log('onClick');
              navigation.goBack();
            },
            component: <HeaderSide text={'Cancel'} />,
          }}
          right={{
            onClick: function (): void {
              handleSave();
            },
            component: <HeaderSide text={'Save'} />,
          }}
        />
        <View style={styles.photoContainer}>
          <ContactPhoto size={100} />
        </View>

        <ContactEditInputSection
          sections={[
            {
              sectionTitle: 'Main information',
              sectionId: '1',
              inputs: [
                {
                  label: 'First Name',
                  placeholder: 'First Name',
                  value: contactData.firstName,
                  onChangeText: (value: string) => {
                    setContactData({ ...contactData, firstName: value });
                  },
                  inputRef: (input: TextInput | null) => {
                    inputs.firstName = input;
                  },
                  returnKeyType: 'next',
                  onSubmitEditing: () => {
                    if (inputs.lastName) {
                      inputs.lastName.focus();
                    }
                  },
                  isRequired: true,
                },
                {
                  label: 'Last Name',
                  placeholder: 'Last Name',
                  value: contactData.lastName,
                  onChangeText: (value: string) => {
                    setContactData({ ...contactData, lastName: value });
                  },
                  inputRef: (input: TextInput | null) => {
                    inputs.lastName = input;
                  },
                  returnKeyType: 'next',
                  onSubmitEditing: () => {
                    if (inputs.email) {
                      inputs.email.focus();
                    }
                  },
                  isRequired: true,
                },
              ],
            },
            {
              sectionTitle: 'Sub Information',
              sectionId: '2',
              inputs: [
                {
                  label: 'Email',
                  placeholder: 'Email',
                  value: contactData.email,
                  onChangeText: (value: string) => {
                    setContactData({ ...contactData, email: value });
                  },
                  inputRef: (input: TextInput | null) => {
                    inputs.email = input;
                  },
                  returnKeyType: 'next',
                  onSubmitEditing: () => {
                    if (inputs.phone) {
                      inputs.phone.focus();
                    }
                  },
                },
                {
                  label: 'Phone',
                  placeholder: 'Phone',
                  value: contactData.phone,
                  onChangeText: (value: string) => {
                    setContactData({ ...contactData, phone: value });
                  },
                  inputRef: (input: TextInput | null) => {
                    inputs.phone = input;
                  },
                  returnKeyType: 'done',
                  onSubmitEditing: () => {
                    handleSave();
                  },
                },
              ],
            },
          ]}
        />
      </View>
    </ScrollView>
  );
}
export default Screen2;

const styles = StyleSheet.create({
  photoContainer: {
    display: 'flex',
    alignItems: 'center',
    paddingVertical: 24,
  },
});
