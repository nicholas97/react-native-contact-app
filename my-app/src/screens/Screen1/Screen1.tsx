import { useEffect, useState } from 'react';
import { ContactModel } from '../../models/ContactModel';
import {
  Dimensions,
  FlatList,
  RefreshControl,
  StyleSheet,
  View,
} from 'react-native';
import ContactListItem from '../../components/ContactListItem';
import Header from '../../components/Header';
import HeaderSide from '../../components/HeaderSide';
import { useNavigation } from '@react-navigation/native';
import { getData } from '../../services/dataService';

function Screen1() {
  const screenHeight = Dimensions.get('window').height;
  const navigation = useNavigation();
  const [contacts, setContacts] = useState<ContactModel[]>([]);
  const [refreshing, setRefreshing] = useState(false);

  const styles = StyleSheet.create({
    container: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },
    listContainer: {
      height: screenHeight - 42,
    },
  });

  const handleRefresh = () => {
    setRefreshing(true);
    fetchContacts();
    console.log('handleRefresh', contacts);
  };
  const handleContactPress = (contact: ContactModel) => {
    navigation.navigate('Screen2', { contact, handleRefresh });
  };
  const fetchContacts = () => {
    setContacts(getData());
  };
  useEffect(() => {
    if (refreshing) {
      setRefreshing(false);
    }
  }, [contacts]);
  useEffect(() => {
    fetchContacts();
  }, []);
  return (
    <View style={styles.container}>
      <View>
        <Header
          title={'Contact'}
          left={{
            onClick: function (): void {
              console.log('onClick');
            },
            component: <HeaderSide icon={'search'} />,
          }}
          right={{
            onClick: function (): void {
              console.log('onClick');
            },
            component: <HeaderSide icon={'plus'} />,
          }}
        />
      </View>
      <View style={styles.listContainer}>
        <FlatList
          data={contacts}
          extraData={contacts}
          keyExtractor={(item) => JSON.stringify(item)}
          renderItem={({ item }) => (
            <ContactListItem
              name={item.firstName + ' ' + item.lastName}
              onPress={() => {
                console.log('ContactListItem on press', item);
                handleContactPress(item);
              }}
            />
          )}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={handleRefresh}
            />
          }
        />
      </View>
    </View>
  );
}
export default Screen1;
