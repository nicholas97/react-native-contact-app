import { View, StyleSheet, Text } from 'react-native';
import ContactEditInputField, {
  ContactEditInputFieldProps,
} from './ContactEditInputField';

interface ContactEditInputSectionProps {
  sections: {
    sectionTitle: string;
    sectionId: string;
    inputs: ContactEditInputFieldProps[];
  }[];
}
function ContactEditInputSection({ sections }: ContactEditInputSectionProps) {
  return (
    <View style={styles.sectionsContainer}>
      {sections.map((section, sectionIndex) => (
        <View
          key={section.sectionId}
          style={styles.sectionContainer}
        >
          <View style={styles.sectionTitleContainer}>
            <Text style={styles.sectionTitleText}>{section.sectionTitle}</Text>
          </View>
          <View style={styles.inputsContainer}>
            {section.inputs.map((input, inputIndex) => (
              <View key={inputIndex}>
                <View style={styles.inputContainer}>
                  <ContactEditInputField {...input} />
                </View>
                {inputIndex != section.inputs.length - 1 && (
                  <View style={styles.inputContainerLine}></View>
                )}

                {sectionIndex + 1 == sections.length &&
                  inputIndex + 1 == section.inputs.length && (
                    <View style={styles.inputContainerLine}></View>
                  )}
              </View>
            ))}
          </View>
        </View>
      ))}
    </View>
  );
}
export default ContactEditInputSection;

const styles = StyleSheet.create({
  sectionsContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  sectionContainer: {
    display: 'flex',
    flexDirection: 'column',
  },
  sectionTitleContainer: {
    paddingHorizontal: 16,
    paddingVertical: 4,
    backgroundColor: 'grey',
  },
  sectionTitleText: {
    fontSize: 24,
    fontWeight: '600',
  },
  inputsContainer: {
    paddingLeft: 16,
  },
  inputContainer: {
    paddingVertical: 8,
    paddingRight: 16,
  },
  inputContainerLine: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
});
