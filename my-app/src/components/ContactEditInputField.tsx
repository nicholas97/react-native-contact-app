import {
  View,
  StyleSheet,
  Text,
  TextInput,
  ReturnKeyTypeOptions,
} from 'react-native';
export interface ContactEditInputFieldProps {
  label?: string;
  placeholder?: string;
  value?: any;
  onChangeText?: (value: any) => void;
  inputRef?: (input: TextInput | null) => void;
  returnKeyType?: ReturnKeyTypeOptions;
  onSubmitEditing?: () => void;
  isRequired?: boolean;
}
function ContactEditInputField({
  label,
  placeholder,
  value,
  onChangeText,
  inputRef,
  returnKeyType,
  onSubmitEditing,
  isRequired = false,
}: ContactEditInputFieldProps) {
  return (
    <View>
      <View style={styles.inputFieldContainer}>
        <View style={styles.labelContainer}>
          <Text style={styles.label}>{label}</Text>
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            ref={(input: TextInput | null) => {
              if (inputRef) {
                inputRef(input);
              }
            }}
            returnKeyType={returnKeyType}
            onSubmitEditing={() => {
              if (onSubmitEditing) {
                onSubmitEditing();
              }
            }}
            style={styles.input}
            value={value}
            onChangeText={onChangeText}
            placeholder={placeholder}
          />
        </View>
      </View>
      {!value && isRequired && (
        <View style={styles.validationFieldContainer}>
          <TextInput style={styles.validationText}>
            {label + ' is required field.'}
          </TextInput>
        </View>
      )}
    </View>
  );
}
export default ContactEditInputField;

const styles = StyleSheet.create({
  validationFieldContainer: {
    marginLeft: 100,
  },
  validationText: {
    color: 'red',
    fontSize: 12,
  },
  inputFieldContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  labelContainer: {
    width: 100,
  },
  label: {
    fontSize: 18,
  },
  inputContainer: {
    flexGrow: 1,
  },
  input: {
    fontSize: 18,
    borderWidth: 1,
    borderColor: 'grey',
    height: 32,
    paddingHorizontal: 8,
    borderRadius: 6,
  },
});
