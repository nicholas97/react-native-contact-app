import React from 'react';
import { Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

interface HeaderSideProps {
  text?: string;
  icon?: string;
}

function HeaderSide({ text, icon }: HeaderSideProps) {
  return (
    <>
      {text && <Text style={styles.text}>{text}</Text>}
      {icon && (
        <Icon
          name={icon}
          size={24}
          color='#ff8c00'
        />
      )}
    </>
  );
}
export default HeaderSide;

const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    fontWeight: '400',
    color: '#ff8c00',
  },
});
