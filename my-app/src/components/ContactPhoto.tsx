import { View, StyleSheet, Text, TextInput } from 'react-native';
interface ContactListItemProps {
  size: number;
}
function ContactPhoto({ size }: ContactListItemProps) {
  const styles = StyleSheet.create({
    photo: {
      width: size,
      height: size,
      backgroundColor: '#ff8c00',
      borderRadius: size / 2,
    },
  });
  return <View style={styles.photo}></View>;
}
export default ContactPhoto;
