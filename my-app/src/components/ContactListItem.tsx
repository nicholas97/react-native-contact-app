import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import ContactPhoto from './ContactPhoto';
interface ContactListItemProps {
  name?: string;
  onPress?: () => void;
}
function ContactListItem({ name, onPress }: ContactListItemProps) {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.itemContainer}>
        <View>
          <ContactPhoto size={64} />
        </View>
        <View style={styles.nameContainer}>
          <Text style={styles.nameText}>{name}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
export default ContactListItem;

const styles = StyleSheet.create({
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    width: '100%',
    paddingVertical: 12,
    marginLeft: 12,
  },
  nameContainer: {
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'center',
    alignContent: 'center',
    paddingLeft: 12,
  },
  nameText: {
    fontSize: 18,
    fontWeight: '400',
  },
});
