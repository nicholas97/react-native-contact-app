import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
interface HeaderProps {
  title?: string;
  left: { onClick: () => void; component?: JSX.Element };
  right: { onClick: () => void; component?: JSX.Element };
}
function Header({ title, left, right }: HeaderProps) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={left.onClick}>
        <View style={styles.sideContainer}>{left.component}</View>
      </TouchableOpacity>
      <View style={styles.centerContainer}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <TouchableOpacity onPress={right.onClick}>
        <View style={styles.sideContainer}>
          <Text>{right.component}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
export default Header;

const styles = StyleSheet.create({
  container: {
    marginTop: 42,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 64,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  sideContainer: {
    paddingHorizontal: 16,
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  centerContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexGrow: 1,
  },
  titleText: {
    fontSize: 32,
    fontWeight: '600',
    width: '100%',
    textAlign: 'center',
  },
});
